const AWS = require('aws-sdk');
const inquirer = require('inquirer');
const fs = require('fs');
const path = require('path');
const { get } = require('http');

// configuring the AWS environment
AWS.config.update({
    accessKeyId: '<Access Key Here>',
    secretAccessKey: '<Secret Access Key Here>'
});

// questions for inquirer, need to figure out how to set these up
const questions = [
    {
        type: 'input',
        name: 'filePath',
        message: 'What is the filePath?',
        
    },
    {
        type: 'confirm',
        name: 'fileAccurate',
        message: 'Is the reported file size accurate for a Stub file? (hint: the average stub file is ~930KB...',
    },
];

// Question Prompt at runtime...
inquirer.prompt(questions[0]).then((answers) => {
    if(answers.filePath){
        doesFileFromPathExist(answers.filePath);
        let fileSize = getFileSizeInKilobytes(answers.filePath);
        
        console.log('File size is: ' + fileSize + 'KB')
        
        inquirer.prompt(questions[1]);
        
        if(answers.fileAccurate){
            console.log('Good, continuing through the process...');
        } else {
            console.log('Either you answered no, or something went wrong... Shutting down...');
        }
    }
});

function doesFileFromPathExist(filePath) {
    if(!fs.existsSync(filePath)) {
        console.log('you entered: ' + filePath);
        console.log('Argument does not exist, try again with a different filepath...');
    } else {
        console.log('File found...');
        return fs.existsSync(filePath);
    }
    
}

function getFileSizeInKilobytes(filePath) {
    let stats = fs.statSync(filePath);
    let fileSizeInBytes = stats.size;
    let fileSizeInKiloBytes = Number(fileSizeInBytes / 1024).toFixed(1);
    
    return fileSizeInKiloBytes;
}

function uploadToS3() {
    let s3 = new AWS.S3();
    // configuring parameters
    let params = {
        Bucket: '<Bucket Name Here>',
        Body : fs.createReadStream(filePath),
        Key : 'folder/'+Date.now()+'_'+path.basename(filePath)
    };

    s3.upload(params, function (err, data) {
        // handle error
        if (err) {
            console.log('Error', err);
        }

        // success
        if (data) {
            console.log('Uploaded in:', data.Location);
        }
    });
}

